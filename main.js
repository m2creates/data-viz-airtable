window.onload = function() {
  let request = new XMLHttpRequest();

  request.open('GET', `https://api.airtable.com/v0/${BASE_NAME}/${TABLE_NAME}?api_key=${API_KEY}`, true);
  request.send();

  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {

      let array = JSON.parse(request.responseText).records;

      var labelsArray = [];
      var dataArray = [];

      array.forEach(function(record, records) {
        const labelName = record.fields[LABEL_NAME];
        const dataName = record.fields[DATA_NAME];

        labelsArray.push(labelName);
        dataArray.push(dataName);
      })

      var ctx = document.getElementById("myChart").getContext('2d');
      Chart.defaults.global.defaultFontColor = "white";

      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: labelsArray,
          datasets: [{
            label: `${DATA_NAME} of ${LABEL_NAME}`,
            data: dataArray,
            backgroundColor: 'rgba(255, 255, 255, 0.2)',
            borderColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 1
          }]
        },
        options: {
          title: {
            display: true,
            fontSize: 20,
            text: CHART_TITLE
          },
          legend: {
            display: true
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });

    };

  }
};

// Form Controls

function openSettings() {
  document.getElementById("settings").classList.toggle("settings-open");
}
function openAbout() {
  document.getElementById("about").classList.toggle("settings-open");
}
document.getElementById("settings-button").addEventListener("click", openSettings);
document.getElementById("about-button").addEventListener("click", openAbout);

var CHART_TITLE;
var API_KEY;
var BASE_NAME;
var TABLE_NAME;
var LABEL_NAME;
var DATA_NAME;

function saveData() {
  localStorage.setItem("inputTitle", CHART_TITLE);
  localStorage.setItem("inputKey", API_KEY);
  localStorage.setItem("inputBase", BASE_NAME);
  localStorage.setItem("inputTable", TABLE_NAME);
  localStorage.setItem("inputLabel", LABEL_NAME);
  localStorage.setItem("inputData", DATA_NAME);
}

var CHART_TITLE = localStorage.getItem("inputTitle");
if (CHART_TITLE == null) {
  CHART_TITLE = "Pokemon per Generation";
}

var API_KEY = localStorage.getItem("inputKey");
if (API_KEY == null) {
  API_KEY = "keyph9gkm2oQ2i7GD";
}

var BASE_NAME = localStorage.getItem("inputBase");
if (BASE_NAME == null) {
  BASE_NAME = "app3Vfib1mRCMnZDp";
}

var TABLE_NAME = localStorage.getItem("inputTable");
if (TABLE_NAME == null) {
  TABLE_NAME = "tblHJoGLi6wmaAWxQ";
}

var LABEL_NAME = localStorage.getItem("inputLabel");
if (LABEL_NAME == null) {
  LABEL_NAME = "Name";
}

var DATA_NAME = localStorage.getItem("inputData");
if (DATA_NAME == null) {
  DATA_NAME = "Amount";
}

function changeData() {
  CHART_TITLE = document.getElementById("chart-input").value;
  API_KEY = document.getElementById("app-input").value;
  BASE_NAME = document.getElementById("base-input").value;
  TABLE_NAME = document.getElementById("table-input").value;
  LABEL_NAME = document.getElementById("label-input").value;
  DATA_NAME = document.getElementById("data-input").value;
  saveData();
}

document.getElementById("settings-form").addEventListener("submit", function(e) {
  e.preventDefault();
  changeData();
  window.location.reload(false);
})
